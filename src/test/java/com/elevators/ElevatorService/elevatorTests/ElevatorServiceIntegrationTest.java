package com.elevators.ElevatorService.elevatorTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.elevators.ElevatorService.model.Elevator;
import com.elevators.ElevatorService.repository.ElevatorRepository;
import com.elevators.ElevatorService.service.ElevatorService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ElevatorServiceIntegrationTest {
	
	@Autowired
	ElevatorService elevatorService;
	
	@Autowired
	ElevatorRepository elevatorRepository;
	
	Elevator testElevator;
	
	@After
	public void tearDown() {
		elevatorRepository.delete(testElevator);
	}
	
	public void testCheckIfShouldBeWorking() {
		
		testElevator = new Elevator(null, 30, 0, 0, true);
		
		testElevator = elevatorRepository.save(testElevator);
		
		elevatorService.checkIfElevatorShouldBeWorking(testElevator);
		
		long countBrokenWorkingElevators = elevatorRepository.findAll().stream()
				.filter(elevator -> elevator.getFloorsLeft() == 0 && elevator.isWorkingOrder())
				.count();
		
		assertEquals(0, countBrokenWorkingElevators);
	}
}
