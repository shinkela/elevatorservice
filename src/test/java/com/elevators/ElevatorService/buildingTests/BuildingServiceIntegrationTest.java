package com.elevators.ElevatorService.buildingTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.elevators.ElevatorService.model.Building;
import com.elevators.ElevatorService.repository.BuildingRepository;
import com.elevators.ElevatorService.service.BuildingService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BuildingServiceIntegrationTest {
	
	@Autowired
	BuildingService buildingService;
	
	@Autowired
	BuildingRepository buildingRepository;
	
	@Test
	public void testFindAll() {
		List<Building> buildingsFromService = buildingService.findAll();
		List<Building> buildingsFromRepo = buildingRepository.findAll();
		
		assertEquals(buildingsFromService.size(), buildingsFromRepo.size());
		
	}

}
