package com.elevators.ElevatorService.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elevators.ElevatorService.model.Elevator;
import com.elevators.ElevatorService.repository.ElevatorRepository;

import Exceptions.NoWorkingOrderElevators;
import Exceptions.NotEnoughFloorsAvailableForYourRide;

@Component
public class ElevatorService {

	@Autowired
	ElevatorRepository elevatorRepository;

	public List<Elevator> findAll(Long buildingId) {
		return elevatorRepository.findAllByBuildingId(buildingId);
	}
	
	public Elevator save(Elevator elevator) {
		return elevatorRepository.save(elevator);
	}

	public Integer rideElevator(Long buildingId, int startFloor, int endFloor) {

		List<Elevator> availableElevators = elevatorRepository.findAllByBuildingIdAndWorkingOrderTrue(buildingId);
		if (availableElevators.isEmpty()) {
			throw new NoWorkingOrderElevators();
		}
		// find first elevator available which could handle number of floors passed
		Elevator potentialElevator = availableElevators
				.stream()
				.map(e -> countCrossedFloors(e, startFloor, endFloor))
				.filter(elevator -> elevator.isAvailableForCurrentRide())
				.findAny().orElse(null);

		if(potentialElevator == null) {
			throw new NotEnoughFloorsAvailableForYourRide();
		}
		
		int totalFloorsMoved = floorMovement(potentialElevator, startFloor, endFloor);
		potentialElevator.floorsMoved(totalFloorsMoved);
		potentialElevator.setCurrentFloor(endFloor);
		checkIfElevatorShouldBeWorking(potentialElevator);
		
		Elevator elevatorSaved = elevatorRepository.save(potentialElevator);
		
		return elevatorSaved.getCode();
	}

	public void checkIfElevatorShouldBeWorking(Elevator elevator) {
//		List<Elevator> allElevators = elevatorRepository.findAll();
//		allElevators.stream()
//					.filter(elevator -> elevator.getFloorsLeft() == 0)
//					.map(e -> e.isWorkingOrder() == false)
//					.collect(Collectors.toList());
		if(elevator.getFloorsLeft() <= 0) {
			elevator.setWorkingOrder(false);
		}
		
	}
	
	private int floorMovement(Elevator elevator, int startFloor, int endFloor) {
		int currentToStart = Math.abs(elevator.getCurrentFloor() - startFloor);
		int startToEnd = Math.abs(startFloor - endFloor);
		int totalFloorsMoved = Math.abs(currentToStart + startToEnd);
		return totalFloorsMoved;
	}

	public Elevator countCrossedFloors(Elevator elevator, int startFloor, int endFloor) {

		int totalFloorsMoved = floorMovement(elevator, startFloor, endFloor);

		if (totalFloorsMoved > elevator.getFloorsLeft()) {
			elevator.setAvailableForCurrentRide(false);
		}
		return elevator;

	}
}
