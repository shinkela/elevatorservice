package com.elevators.ElevatorService.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elevators.ElevatorService.model.Building;
import com.elevators.ElevatorService.repository.BuildingRepository;

@Component
public class BuildingService {
	
	@Autowired
	BuildingRepository buildingRepository;
	
	
	public List<Building> findAll() {
		return buildingRepository.findAll();
	}
	
	public Building create(Building building) {
		return buildingRepository.save(building);
	}
}
