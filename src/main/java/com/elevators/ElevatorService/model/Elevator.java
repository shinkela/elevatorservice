package com.elevators.ElevatorService.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PostUpdate;

@Entity
public class Elevator {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
//	@NotNull(message = "Please place the elevator's code")
//	@Column(unique = true)
	private int code;
	private int currentFloor;
	private int floorsLeft;
	private boolean workingOrder;
	private boolean isAvailableForCurrentRide;
	@ManyToOne(fetch = FetchType.EAGER)
	private Building building;

	public Elevator() {

	}

	public Elevator(Long id, int code, int currentFloor, int floorsLeft, boolean workingOrder) {
		super();
		this.id = id;
		this.code = code;
		this.currentFloor = currentFloor;
		this.floorsLeft = floorsLeft;
		this.workingOrder = workingOrder;
	}
	
	public void floorsMoved(int number) {
		int floorsLeft = this.floorsLeft - number;
		this.floorsLeft = floorsLeft < 0 ? 0 : floorsLeft;
		}
	
	@PostUpdate
	public void resetAvailability() {
		this.isAvailableForCurrentRide = true;
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getCurrentFloor() {
		return currentFloor;
	}

	public void setCurrentFloor(int currentFloor) {
		this.currentFloor = currentFloor;
	}

	public int getFloorsLeft() {
		return floorsLeft;
	}

	public void setFloorsLeft(int floorsLeft) {
		this.floorsLeft = floorsLeft;
	}

	public boolean isWorkingOrder() {
		return workingOrder;
	}

	public void setWorkingOrder(boolean workingOrder) {
		this.workingOrder = workingOrder;
	}

	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

	public boolean isAvailableForCurrentRide() {
		return isAvailableForCurrentRide;
	}

	public void setAvailableForCurrentRide(boolean isAvailableForCurrentRide) {
		this.isAvailableForCurrentRide = isAvailableForCurrentRide;
	}

}
