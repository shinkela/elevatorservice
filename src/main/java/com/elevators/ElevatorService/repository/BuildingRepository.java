package com.elevators.ElevatorService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.elevators.ElevatorService.model.Building;

@Repository
public interface BuildingRepository extends JpaRepository<Building, Long>{
	
}
