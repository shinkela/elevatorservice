package com.elevators.ElevatorService.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.elevators.ElevatorService.model.Elevator;

@Repository
public interface ElevatorRepository extends JpaRepository<Elevator, Long> {
	
	public List<Elevator> findAllByBuildingId(Long buildingId);
	
	public List<Elevator> findAllByBuildingIdAndWorkingOrderTrue(Long buildingId);



}
