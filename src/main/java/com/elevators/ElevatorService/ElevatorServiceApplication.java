package com.elevators.ElevatorService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import Exceptions.NoWorkingOrderElevators;


@SpringBootApplication
public class ElevatorServiceApplication {

	public static void main(String[] args) {
		try {
			SpringApplication.run(ElevatorServiceApplication.class, args);
		} catch (NoWorkingOrderElevators e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}