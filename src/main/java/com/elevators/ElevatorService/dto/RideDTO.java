package com.elevators.ElevatorService.dto;

public class RideDTO {
	
	private Long buildingId;
	private int startFloor;
	private int endFloor;
	
	public RideDTO() {
		
	}

	public RideDTO(Long buildingId, int startFloor, int endFloor) {
		super();
		this.buildingId = buildingId;
		this.startFloor = startFloor;
		this.endFloor = endFloor;
	}

	public Long getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(Long buildingId) {
		this.buildingId = buildingId;
	}

	public int getStartFloor() {
		return startFloor;
	}

	public void setStartFloor(int startFloor) {
		this.startFloor = startFloor;
	}

	public int getEndFloor() {
		return endFloor;
	}

	public void setEndFloor(int endFloor) {
		this.endFloor = endFloor;
	}
	
	
}
