package com.elevators.ElevatorService.dto;

import com.elevators.ElevatorService.model.Elevator;

public class ElevatorDTO {

	private Long id;
	private int code;
	private int currentFloor;
	private int floorsLeft;
	private boolean workingOrder;
	private BuildingDTO buildingDTO;

	public ElevatorDTO() {

	}

	public ElevatorDTO(Elevator elevator) {
		this.id = elevator.getId();
		this.code = elevator.getCode();
		this.currentFloor = elevator.getCurrentFloor();
		this.floorsLeft = elevator.getFloorsLeft();
		this.workingOrder = elevator.isWorkingOrder();
		this.buildingDTO = new BuildingDTO(elevator.getBuilding());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getCurrentFloor() {
		return currentFloor;
	}

	public void setCurrentFloor(int currentFloor) {
		this.currentFloor = currentFloor;
	}

	public int getFloorsLeft() {
		return floorsLeft;
	}

	public void setFloorsLeft(int floorsLeft) {
		this.floorsLeft = floorsLeft;
	}

	public boolean isWorkingOrder() {
		return workingOrder;
	}

	public void setWorkingOrder(boolean workingOrder) {
		this.workingOrder = workingOrder;
	}

	public BuildingDTO getBuildingDTO() {
		return buildingDTO;
	}

	public void setBuildingDTO(BuildingDTO buildingDTO) {
		this.buildingDTO = buildingDTO;
	}

}
