package com.elevators.ElevatorService.dto;


import com.elevators.ElevatorService.model.Building;

public class BuildingDTO {

	private Long id;
	private String name;
	private int numberOfFloors;
//	private List<ElevatorDTO> elevators;

	public BuildingDTO() {
		
	}
	
	public BuildingDTO(Building building) {
		this.id = building.getId();
		this.name = building.getName();
		this.numberOfFloors = building.getNumberOfFloors();
//		this.elevators = building.getElevators()
//				.stream()
//				.map(ElevatorDTO::new)
//				.collect(Collectors.toList());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfFloors() {
		return numberOfFloors;
	}

	public void setNumberOfFloors(int numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}

	

}
