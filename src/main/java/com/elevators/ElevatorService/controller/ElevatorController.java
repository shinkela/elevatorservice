package com.elevators.ElevatorService.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elevators.ElevatorService.dto.ElevatorDTO;
import com.elevators.ElevatorService.dto.RideDTO;
import com.elevators.ElevatorService.service.ElevatorService;

@RestController
@RequestMapping("buildings/{buildingId}/elevators")
public class ElevatorController {

	@Autowired
	ElevatorService elevatorService;

	@GetMapping("")
	public ResponseEntity<?> findAll(@PathVariable Long buildingId) {

		List<ElevatorDTO> elevators = elevatorService.findAll(buildingId).stream().map(ElevatorDTO::new)
				.collect(Collectors.toList());
		return new ResponseEntity<>(elevators, HttpStatus.OK);
	}

	@PostMapping("/ride")
	public ResponseEntity<?> rideElevator(@RequestBody RideDTO rideDTO, @PathVariable Long buildingId) {
		Integer elevatorNumber = elevatorService.rideElevator(rideDTO.getBuildingId(), rideDTO.getStartFloor(),
				rideDTO.getEndFloor());

		if (elevatorNumber == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(elevatorNumber, HttpStatus.OK);
	}
}
