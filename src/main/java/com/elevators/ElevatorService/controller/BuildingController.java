package com.elevators.ElevatorService.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elevators.ElevatorService.dto.BuildingDTO;
import com.elevators.ElevatorService.model.Building;
import com.elevators.ElevatorService.service.BuildingService;
import com.elevators.ElevatorService.service.MapValidationErrorService;

@RestController
@RequestMapping("/buildings")
public class BuildingController {

	@Autowired
	BuildingService buildingService;
	@Autowired
	MapValidationErrorService errorService;

	@GetMapping("")
	public ResponseEntity<?> getAllBuildings() {

		List<BuildingDTO> retVal = buildingService.findAll().stream().map(BuildingDTO::new)
				.collect(Collectors.toList());

		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<?> create(@Valid @RequestBody Building building, BindingResult result) {

		ResponseEntity<Map<String, String>> errors = errorService.mapValidationErrorService(result);
		if (errors != null) 	return errors;
		Building saved = buildingService.create(building);

		return new ResponseEntity<>(saved, HttpStatus.CREATED);
	}

}
