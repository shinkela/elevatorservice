package Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NoWorkingOrderElevators extends RuntimeException {
		
	public static String MESSAGE = "There are no elevators in working order.";
	public NoWorkingOrderElevators() {
		super(MESSAGE);
	}
}
