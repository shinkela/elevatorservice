package Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NotEnoughFloorsAvailableForYourRide extends RuntimeException {
	
	public static String MESSAGE = "There are not enough floor movements available for your ride. ";
	public NotEnoughFloorsAvailableForYourRide() {
		super(MESSAGE);
	}
}
